import functools


def print_string(m, n):
    return print(m * n)


def biggest(m, n):
    return m if len(m) > len(n) else n


def mirror(int):
    return int [::-1]


def swap_last_first(int):
    return int[-1] + int[1:-1:] + int[0]


def is_palindrome(int):
    return int[::-1] == int


def factorial(n):
    return n * factorial(n - 1) if n > 1 else 1


def sum_list(lista):
    return lista[0] + sum_list(lista[1::]) if len(lista) > 1 else lista[0]


def smallest(lista):
    return functools.reduce(lambda x, y: x if x < y else y, lista)


def union(lista, listb):
    return lista + [x for x in listb if x not in lista]


def make_odds(size, start):
    return [x for x in range(start, start + size * 2) if x % 2 == 1]


def memoize(f):
    cache = {}
    return lambda *args: cache[args] if args in cache else cache.update({args: f(*args)}) or cache[args]

@memoize
def fib(n):
    return n if n < 2 else fib(n-2) + fib(n-1)