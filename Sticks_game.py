sticks_initial = 20
sticks = sticks_initial
counter = 0
while sticks > 1:
    for _ in range(5): 
    print('|    ' * sticks)
    sticks_removed = int(input('how many sticks you want to remove'))
    if sticks_removed > 3 or sticks_removed < 1:
        print('you can only choose up to 3 sticks at the time')
    elif sticks_removed >= sticks:
        print('you are taking more sticks than there are')
    else:
        sticks -= sticks_removed
        if counter % 2 == 0:
            print("player A took {} sticks. There are {} left".format(sticks_removed, sticks))
        else :
            print("player B took {} sticks. There are {} left".format(sticks_removed, sticks))
        counter += 1
if counter % 2 == 0:
    print('player B wins')
else:
    print('player A wins')