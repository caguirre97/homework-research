# import library to be more exact
from decimal import*
principal = input('Please type the amount of money borrowed, a positive int (no commas): ')
years = input('Please type the number of years used to repay the loan, a positive int: ')
rate = input(
    'Please type the annual interest rate charged on the principal,\
a fraction. (e.g., 8.5% is typed as 0.085). ')

# setting annual payment to the function given
annual_payment = "{}".format((1 + Decimal(rate)) ** Decimal(years) *\
                             Decimal(principal) * Decimal(rate) /\
                             ((1 + Decimal(rate)) ** Decimal(years) - 1))

# monthly payment and total variables
monthly_payment = "{}".format(Decimal(annual_payment) / 12)
total = "{}".format(Decimal(annual_payment) * Decimal(years))

print('The annual payment is $' + annual_payment)
print('The monthly payment is $' + monthly_payment)
print('The total paid over the life of the loan is $' + total)